ActiveAdmin.register Page do
  before_filter do
    Page.class_eval do
      def to_param
        id.to_s
      end
    end
  end

  # permit_params :title, :slug, :heading, :body, :image, :menu, :menu_label, category_ids: []
  permit_params :title, :slug, :heading, :body, :image, :menu, :menu_label, category_ids: []

  index do
    column :id
    column :title
    column :slug
    column :menu
    column :menu_label
    # column :heading
    # column :body do |a|
    #   truncate(a.body, omision: "...", length: 50, escape: false)
    # end
    column :created_at
    column :updated_at
    actions
  end

  show do |page|
    attributes_table do
      row :id
      row :title
      row :slug
      row :menu
      row :menu_label
      row :heading
      row (:body) { |p| raw(p.body) }
      row :created_at
      row :updated_at
    end
    panel 'Nuotrauka' do
      attributes_table_for page do
        row :image do
          image_tag page.image.url(:md)
        end
      end
    end
    panel 'Kategorijos' do
      attributes_table_for page do
        row :categories do |page|
          page.categories.map { |c| c.name }.join(", ")
        end
      end
    end
  end

  form multipart: true do |f|
    f.inputs do
      f.input :title
      f.input :slug
      f.input :heading
      f.input :body, input_html:{class: 'redactor'}
    end
    f.inputs :multipart => true do 
      f.input :image, :as => :file, :hint => f.object.image.present? \
        ? image_tag(f.object.image.url(:md))
        : content_tag(:span, "")
      f.input :image_cache, :as => :hidden 
    end
    f.inputs do
      f.input :menu 
      f.input :menu_label
    end
    f.inputs do
      f.input :categories, as: :check_boxes
    end
    f.actions
  end
end
