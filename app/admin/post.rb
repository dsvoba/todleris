ActiveAdmin.register Post do

  permit_params :user_id, :topic_id, :body, :image

  index do
    column :id
    column :user_id do |u|
      User.find(u.user_id).email
    end
    column :body do |a|
      truncate(strip_tags(a.body), omision: "...", length: 50, escape: false)
    end
    column :image do |article|
      article.image.url ? "Yes" : "No"
    end
    column :published_at
    column :updated_at
    actions
  end

  show do |article|
    attributes_table do
      row :id
      row :user_id
      row (:body) { |a| raw(a.body) }
      row :created_at
      row :updated_at
    end
    panel 'Nuotrauka' do
      attributes_table_for article do
        row :image do
          image_tag article.image.url(:md)
        end
      end
    end
  end

  form multipart: true do |f|
    f.inputs do
      f.input :user_id, as: :select, collection: User.all.map{|u| ["#{u.email}", u.id]}
      f.input :topic_id, as: :select, collection: Topic.all.map{|t| ["#{t.subject}", t.id]}
      f.input :body
    end
    f.inputs multipart: true do 
      f.input :image, as: :file, hint: f.object.image.present? \
        ? image_tag(f.object.image.url(:md))
        : content_tag(:span, "")
      f.input :image_cache, as: :hidden 
    end
    f.actions
  end

end
