ActiveAdmin.register Material do
  permit_params :name, :body, :image, :image_cache, material_attachments_attributes: [:id, :file, :_destroy]

  show do |material|
    attributes_table do
      row :name
      row :body
    end

    panel 'Failai' do
      attributes_table_for material do
        row :material_attachments do |material|
          material.material_attachments.map { |c| File.basename c.file.to_s }.join("<br /> ").html_safe
        end
      end
    end

  end
  
  form html: { multipart: true }  do |f|
    f.inputs do
      f.input :name
      f.input :body
    end
    
    f.inputs multipart: true do 
     f.input :image, as: :file, hint: f.object.image.present? \
       ? image_tag(f.object.image.url(:md))
       : content_tag(:span, "")
     f.input :image_cache, :as => :hidden
    end

    f.inputs 'Attachments' do
      f.has_many :material_attachments do |p|
        p.input :file, as: :file, label: 'Failas', hint: (File.basename p.object.file.to_s)
        p.input :_destroy, as: :boolean, required: false, label: 'Remove file' unless p.object.new_record?
      end
    end
    f.actions
  end
end
