ActiveAdmin.register Article do
  permit_params :title, :body, :published_at, :draft, :image, category_ids: []

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_articles_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_articles_path }
      end
    end
  end

  index do
    column :id
    column :title
    # column :body do |a|
    #   truncate(strip_tags(a.body), omision: "...", length: 50, escape: false)
    # end
    column :image do |article|
      article.image.url ? "Yes" : "No"
    end
    column :draft
    column :published_at
    # column :created_at
    column :updated_at
    actions
  end

  show do |article|
    attributes_table do
      row :id
      row :title
      row (:body) { |a| raw(a.body) }
      row :published_at
      row :draft
      row :created_at
      row :updated_at
    end
    panel 'Nuotrauka' do
      attributes_table_for article do
        row :image do
          image_tag article.image.url(:md)
        end
      end
    end
    panel 'Kategorijos' do
      attributes_table_for article do
        row :categories do |article|
          article.categories.map { |c| c.name }.join(", ")
        end
      end
    end
  end

  form multipart: true do |f|
    f.inputs do
      f.input :title, input_html: { maxlength: 45 }
      f.input :body, input_html:{class: 'redactor'}
      f.input :published_at
      f.input :draft, as: :boolean
    end
    f.inputs :multipart => true do 
      f.input :image, :as => :file, :hint => f.object.image.present? \
        ? image_tag(f.object.image.url(:md))
        : content_tag(:span, "")
      f.input :image_cache, :as => :hidden 
    end
    f.inputs do
      f.input :categories, as: :check_boxes
    end
    f.actions
  end
end
