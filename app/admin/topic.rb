ActiveAdmin.register Topic do

  permit_params :user_id, :subject, :body, :locked, :pinned, :hidden, :approved

  index do
    column :id
    column :user
    column :subject
    column :body do |a|
      truncate(a.body, omision: "...", length: 50, escape: false)
    end
    # column :locked
    # column :pinned
    # column :hidden
    column :approved
    column :slug
    column :created_at
    column :updated_at
    actions
  end

  show do |article|
    attributes_table do
      row :id
      row :user
      row :subject
      row (:body) { |a| raw(a.body) }
      # row :locked
      # row :pinned
      # row :hidden
      row :approved
      row :slug
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :user_id, as: :select, collection: User.all.map{|u| ["#{u.email}", u.id]}
      f.input :subject
      f.input :body
      # f.input :locked
      # f.input :pinned
      # f.input :hidden
      f.input :approved, as: :boolean, input_html: { checked: 'checked' }
      f.input :slug
    end
    f.actions
  end

end
