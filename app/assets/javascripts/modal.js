document.addEventListener('DOMContentLoaded', function(){
  if (location.hash.substring(0, 6) == "#modal") {
    if ($(location.hash).length > 0) {
      $('body').addClass('modal-open'); 
    }
  }
  
  function modalClose() {
      if (location.hash.substring(0, 6) == "#modal") {
          location.hash = '';
          history.pushState("", document.title, window.location.pathname);
      }
  }

  document.addEventListener('keyup', function(e) {
      if (e.keyCode == 27) {
          modalClose();
      }
  });

  var modal = document.querySelectorAll('.modal');
  var close = document.querySelectorAll('.modal .close');

  for (var i = 0; i < close.length; i++) {
    close[i].addEventListener('click', function(e) {
        modalClose();
    }, false);
  }

  for (var i = 0; i < modal.length; i++) {
    modal[i].addEventListener('click', function(e) {
        modalClose();
    }, false);
  }

  for (var i = 0; i < close.length; i++) {
    modal[i].children[0].addEventListener('click', function(e) {
        e.stopPropagation();
    }, false);
  }

  window.addEventListener('hashchange', function(e) {
    if (location.hash.substring(0, 6) == "#modal") { 
      $('body').addClass('modal-open');
    } else {
      $('body').removeClass('modal-open');
    }
  }, false);
}, false);
