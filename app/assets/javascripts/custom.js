$(document).ready(function() {
  // new post image indicator
  $("body").on("change", "#uploadPostImage", function () {
    $(this).parent().addClass('active');
  });

  // Scroll to new answer form
  $('.topics-show .inner-top .actions a').on('click', function(e) { 
    var el = $( e.target.getAttribute('href') );
    var elOffset = el.offset().top;
    var elHeight = el.height();
    var windowHeight = $(window).height();
    var offset;

    if (elHeight < windowHeight) {
      offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
    }
    else {
      offset = elOffset;
    }
    var speed = 500;
    $('html, body').animate({scrollTop:offset}, speed);
  });

  // This code work with those browser which does not support required (html5) attribute
  $("form").submit(function(e) {
    var ref = $(this).find("[required]");
    $(ref).each(function(){
      if ( $(this).val() == '' )
      {
        // alert("Required field should not be blank.");
        $(this).wrap( "<div class='field_with_errors'></div>" );
        $(this).focus();
        e.preventDefault();
        return false;
      }
    });  return true;
  });

  // setTimeout(function() {
  //   $(".alert").fadeOut(500)
  // }, 3000);
  
  // mobile navigation
  $("#nav-mobile").html($("#nav-main").html());
  $("#nav-trigger span").click(function(){
      if ($("nav#nav-mobile ul").hasClass("expanded")) {
          $("nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
          $(this).removeClass("open");
      } else {
          $("nav#nav-mobile ul").addClass("expanded").slideDown(250);
          $(this).addClass("open");
      }
  });

});

function numbersonly(myfield, e, dec) { 
  var key; 
  var keychar; 
  
  if (window.event) 
    key = window.event.keyCode; 
  else if (e) 
    key = e.which; 
  else 
    return true; 
  
  keychar = String.fromCharCode(key); 
  
  // control keys 
  if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) ) 
    return true; 
  // numbers 
  else if ((("0123456789").indexOf(keychar) > -1)) 
    return true; 
  // decimal point jump 
  else if (dec && (keychar == ".")) { 
    myfield.form.elements[dec].focus(); 
    return false;
  }
  else
    return false; 
}
