jQuery(function() {
  if ($('#infinite-scrolling').size() > 0) {
    $('.pagination').hide('');
    $(window).scroll(function() {
      var url;
      if (window.pagination_loading) {
        return;
      }
      url = $('.pagination .next_page').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 50) {
        window.pagination_loading = true;
        $('.pagination').show().text('Kraunama ...');
        return $.getScript(url).always(function() {
          return window.pagination_loading = false;
        });
      }
    });
  }
});