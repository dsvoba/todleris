class WelcomeController < ApplicationController
  def index
    @articles = Article.published.paginate(page: params[:page], per_page: 10).order('created_at DESC')
    respond_to do |format|
      format.html
      format.js
    end
  end
end
