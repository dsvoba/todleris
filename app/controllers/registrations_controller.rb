class RegistrationsController < Devise::RegistrationsController
  respond_to :html, :js

  def create
    session[:registration_params].deep_merge!(sign_up_params) if params[:user]
    @user = User.new(session[:registration_params])
    @user.current_step = session[:registration_step]

    if params[:back_button]
      @user.previous_step
    elsif @user.valid?
      if @user.last_step?
        @user.save# if @user.all_valid?
        yield @user if block_given?
      else
        @user.next_step
      end
      session[:registration_step] = @user.current_step
    end
    
    if @user.new_record?
      respond_to do |format|
        format.html
        format.js
      end
    else
      session[:registration_step] = session[:registration_params] = nil

      if @user.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, @user)
        respond_to do |format|
          format.html
          format.js
        end
      else
        set_flash_message :notice, :"signed_up_but_#{@user.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_to do |format|
          format.html
          format.js
        end
      end
    end
  end

  private

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :dob, :location, :status, :phone, children_attributes: [:first_name, :gender, :dob])
  end

  def account_update_params
    params.require(:user).permit(:email, :password, :password_confirmation, :current_password, :first_name, :dob, :location, :status, :phone, :avatar, :avatar_cache, :remove_avatar)
  end
end