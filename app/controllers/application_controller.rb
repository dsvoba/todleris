class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def set_admin_locale
    I18n.locale = :en
  end

  before_action :setup_registration_steps

  def setup_registration_steps
    unless user_signed_in?
      session[:registration_params] ||= {}
      @user = User.new(session[:registration_params])
      @user.current_step = session[:registration_step]
    end
  end
end
