class PostsController < InheritedResources::Base
  before_filter :authenticate_user!
  actions :create, :destroy
  before_action :ensure_ownership, only: [:create, :destroy]
  respond_to :html, :js  

  def user
    @user_posts = current_user.posts
  end

  def create
    @post = Post.new(post_params)
    @topic = Topic.find(params[:topic_id])
    @post.topic_id = @topic.id
    create!
  end

  private

    def ensure_ownership
      if current_user != User.find(post_params[:user_id] || params[:user_id])
        redirect_to :back, alert: 'Ownership error!'
      end
    end

    def post_params
      params.fetch(:post, {}).permit(:user_id, :body, :image)
    end
end
