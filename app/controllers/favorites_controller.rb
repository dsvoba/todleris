class FavoritesController < ApplicationController
  before_filter :authenticate_user!
  
  def user
    @favorites = current_user.favorites.where(favoritable_type: "Topic")
    @user_favorite_topics = []
    @favorites.each do |f|
      @user_favorite_topics << f.favoritable_type.classify.constantize.find(f.favoritable_id)
    end
  end

  def create
    @favorite = current_user.favorites.build
    params.each do |name, value|
      if name =~ /(.+)_id$/
        @favorite.favoritable_id = value
        @favorite.favoritable_type = $1.capitalize
      end
    end

    respond_to do |format|
      if @favorite.save
        format.html {redirect_to topic_path(Topic.find(@favorite.favoritable_id)), notice: "Klausimas įtrauktas į Jūsų mėgstamiausių sąrašą"}
        format.js { render action: "create_#{@favorite.favoritable_type.downcase}" }
      else
        format.html {redirect_to topic_path(Topic.find(@favorite.favoritable_id)), alert: "Klausimo įtraukti į mėgstamiausių sąrašą nepavyko!"}
        format.js { render action: "create_#{@favorite.favoritable_type.downcase}" }
      end
    end
  end
  
  def destroy
    @favorite = current_user.favorites.find(params[:id])
    @obj = @favorite.favoritable_type.classify.constantize.find(@favorite.favoritable_id)

    respond_to do |format|
      if @favorite.destroy
        format.html {redirect_to topic_path(@topic), notice: "Klausimas pašalintas iš Jūsų mėgstamiausių sąrašo"}
        format.js { render action: "destroy_#{@obj.class.name.downcase}" }
      else
        format.html {redirect_to topic_path(@topic), alert: "Klausimo iš mėgstamiausių sąrašo pašalinti nepavyko!"}
        format.js { render action: "destroy_#{@obj.class.name.downcase}" }
      end
    end
  end
end
