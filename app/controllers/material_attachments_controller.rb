class MaterialAttachmentsController < ApplicationController
  before_filter :authenticate_user!

  def download
    file_path = "#{Rails.root}/public/uploads/material_attachment/file/#{params[:id]}/#{params[:basename]}.#{params[:extension]}"
    send_file(file_path, type: MIME::Types.type_for(File::basename file_path).first.content_type, disposition: 'attachment')
  end
end
