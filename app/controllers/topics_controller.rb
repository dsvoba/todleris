class TopicsController < InheritedResources::Base
  before_filter :authenticate_user!
  actions :index, :show, :create, :destroy
  before_action :ensure_ownership, only: [:create, :destroy]
  respond_to :html, :js

  def index
    @topics = Topic.visible.paginate(page: params[:page], per_page: 10).order(created_at: :desc)
  end

  def user
    @topics = current_user.topics.visible.order(created_at: :desc)
  end

  def show
    @post = Post.new
    @topic = Topic.friendly.find(params[:id])
    @posts = @topic.posts
    @favorite = current_user.favorites.find_by(favoritable_id: @topic.id, favoritable_type: @topic.class.name) || current_user.favorites.build(favoritable_id: @topic.id)
    show!
  end

  private

    def ensure_ownership
      if current_user != User.find(topic_params[:user_id] || params[:user_id])
        redirect_to :back, alert: 'Ownership error!'
      end
    end

    def topic_params
      params.fetch(:topic, {}).permit(:user_id, :subject, :body, :locked, :pinned, :hidden, :approved, :views_count)
    end
end
