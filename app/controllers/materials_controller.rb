class MaterialsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @materials = Material.paginate(page: params[:page], per_page: 10).order(created_at: :desc)
  end
end
