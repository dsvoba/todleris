class SessionsController < Devise::SessionsController
	respond_to :html, :js
  
	def create
    resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    sign_in(resource_name, resource)

    respond_to do |format|
      format.html
      format.js
    end
	end

	def failure
    @failure = "Neteisingai suvestas el. paštas arba slaptažodis"
    respond_to do |format|
      format.html
      format.js
    end
  end
end
