module PostsHelper
  def find_or_build_favorite(post)
    current_user.favorites.find_by(favoritable_id: post.id, favoritable_type: post.class.name) || current_user.favorites.build(favoritable_id: post.id)
  end
end
