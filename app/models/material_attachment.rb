class MaterialAttachment < ActiveRecord::Base
  belongs_to :material  
  mount_uploader :file, FileUploader
end
