require 'friendly_id'

class Topic < ActiveRecord::Base
  belongs_to :user
  has_many :posts, -> { order "posts.created_at DESC"}, dependent: :destroy
  has_many :favorites, as: :favoritable

  accepts_nested_attributes_for :posts
  
  before_validation :approve_topic
  
  validates :subject, presence: true, length: { maximum: 255 }
  validates :body, presence:true
  validates :user_id, presence: true
  
  scope :visible, -> {where.not(hidden: true)}

  extend FriendlyId
  friendly_id :subject, use: [:slugged, :finders]
  
  class << self
    def by_pinned_or_most_recent_post
      order('topics.pinned DESC').
      order('topics.last_post_at DESC').
      order('topics.id')
    end
  end
  
  private
  
    def approve_topic
      self.approved = true
    end
end
