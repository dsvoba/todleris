class Category < ActiveRecord::Base
  has_and_belongs_to_many :articles
  has_and_belongs_to_many :pages
  
  scope :has_articles, -> {where('EXISTS(SELECT 1 FROM articles_categories WHERE category_id = categories.id)')}
end
