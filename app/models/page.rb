class Page < ActiveRecord::Base
  has_and_belongs_to_many :categories
  
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  mount_uploader :image, ImageUploader

  validates_presence_of :title
  validates :menu_label, presence: true, :if => :menu?
end
