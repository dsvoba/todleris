class Child < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :gender, :dob
end
