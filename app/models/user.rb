class User < ActiveRecord::Base  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :children, dependent: :destroy
  has_many :topics
  has_many :posts
  has_many :favorites, inverse_of: :user

  accepts_nested_attributes_for :children

  attr_writer :current_step

  validates_presence_of :first_name, on: :update 
  # validates_presence_of :email, :password, :password_confirmation, :if => lambda { |o| o.current_step == "account" }
  validates_presence_of :first_name, :dob, :location, :if => lambda { |o| o.current_step == "user" }
  validates_presence_of :status, :if => lambda { |o| o.current_step == "status" }
  validates_associated :children, :if => lambda { |o| o.current_step == "pregnant" or o.current_step == "parent"}
  validates_presence_of :phone, :if => lambda { |o| o.current_step == "confirmation" }

  mount_uploader :avatar, AvatarUploader

  # validates_presence_of   :avatar
  validates_integrity_of  :avatar
  validates_processing_of :avatar

  def current_step
    @current_step || steps.first
  end

  def steps
    %w[account user status confirmation]
  end

  def next_step
    if self.status.present? and self.status != "childless" and self.current_step == "status"
      self.current_step = self.status
    elsif self.current_step == "pregnant" or self.current_step == "parent"
      self.current_step = steps.last
    else
      self.current_step = steps[steps.index(current_step)+1]
    end
  end

  def previous_step
    # Cia reikia prideti case funkcija, kad butu imanoma grysti ir tarpiniu zingsniu (parent, pregnant)
    self.current_step = steps[steps.index(current_step)-1]
  end

  def first_step?
    current_step == steps.first
  end

  def last_step?
    current_step == steps.last
  end

  def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end
end
