class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic
  has_many :favorites, as: :favoritable, dependent: :destroy

  validates_presence_of :user_id, :topic_id, :body

  after_create :set_topic_last_post_at

  mount_uploader :image, ImageUploader

  private

    def set_topic_last_post_at
      topic.update_column(:last_post_at, created_at)
    end
end
