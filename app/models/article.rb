class Article < ActiveRecord::Base
  has_and_belongs_to_many :categories
  
  before_validation :set_default_date
  validates_presence_of :title, :body, :published_at

  # scope :published, -> {where("draft = ?", false).where("published_at <= ?", Time.now).where('EXISTS(SELECT 1 FROM articles_categories WHERE article_id = articles.id)')}
  scope :published, -> {where(draft: false).where("published_at <= ?", Time.current).order(published_at: :desc)}

  def self.in_categories(category_ids=[])
    return all if category_ids.blank? || category_ids.empty?
    includes(:categories).where(categories: { id: category_ids })
  end

  mount_uploader :image, ImageUploader

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  private

  def set_default_date
    self.published_at ||= Time.current
  end
end
