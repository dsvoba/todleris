class Material < ActiveRecord::Base
  has_many :material_attachments, dependent: :destroy

  validates_presence_of :name, :body

  accepts_nested_attributes_for :material_attachments, allow_destroy: true
  
  mount_uploader :image, ImageUploader
end
