Rails.application.routes.draw do
  mount RedactorRails::Engine => '/redactor_rails'

  get "/users/sign_up", to: redirect("/#modal-new-registration")
  get "/users/sign_in", to: redirect("/#modal-new-session")

  devise_for :users, controllers: {sessions: "sessions", registrations: "registrations"}

  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  resources :articles, only: [:show]
  resources :pages, only: [:show]
  resources :materials, only: [:index]
  resources :topics do
    collection {get :user}
    resources :posts
  end
  resources :posts do
    collection {get :user}   
  end
  resources :favorites, only: [:create, :destroy] do
    collection {get :user}
  end

  get "/biblioteka/atsisiusti/:id/:basename.:extension" => "material_attachments#download"

  root 'welcome#index'
end
