class CreateCategoriesPages < ActiveRecord::Migration
  def change
    create_table :categories_pages do |t|
      t.references :category, index: true, foreign_key: true
      t.references :page, index: true, foreign_key: true
    end
  end
end
