class AddSlugToTopics < ActiveRecord::Migration
  def change
    add_column :topics, :slug, :string
  end

  def change
    add_column :topics, :slug, :string
    add_index :topics, :slug, unique: true
    Topic.reset_column_information
    Topic.find_each(&:save)
  end
end
