class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.references :user, index: true, foreign_key: true
      t.string :subject
      t.text :body
      t.boolean  :locked, default: false, null: false
      t.boolean  :pinned, default: false
      t.boolean  :hidden, default: false
      t.boolean :approved
      t.integer :views_count, default: 0
      t.datetime :last_post_at

      t.timestamps null: false
    end
  end
end
