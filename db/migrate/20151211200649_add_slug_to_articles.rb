class AddSlugToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :slug, :string
  end
  
  def change
    add_column :articles, :slug, :string
    add_index :articles, :slug, unique: true
    Article.reset_column_information
    Article.find_each(&:save)
  end
end
