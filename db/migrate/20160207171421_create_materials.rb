class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :name
      t.text :body
      t.string :image

      t.timestamps null: false
    end
  end
end
