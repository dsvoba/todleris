class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :dob, :date
    add_column :users, :location, :string
    add_column :users, :status, :string
    add_column :users, :phone, :string
  end
end
