class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.string :heading
      t.text :body
      t.string :image
      t.boolean :menu, default: false
      t.string :menu_label

      t.timestamps null: false
    end
  end
end
