class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.references :user, index: true, foreign_key: true
      t.string :first_name
      t.string :gender
      t.date :dob

      t.timestamps null: false
    end
  end
end
