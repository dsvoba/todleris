class CreateMaterialAttachments < ActiveRecord::Migration
  def change
    create_table :material_attachments do |t|
      t.references :material, index: true, foreign_key: true
      t.string :file

      t.timestamps null: false
    end
  end
end
