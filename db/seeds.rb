# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#

# Admin user
unless AdminUser.find_by(email: "admin@todleris.lt")
  puts "Creating admin users"
  AdminUser.create!(email: 'admin@todleris.lt', password: 'testas12', password_confirmation: 'testas12')
end

unless User.find_by(email: "admin@todleris.lt")
  puts "Creating forum user"
  User.create!({
    email: 'admin@todleris.lt', 
    password: 'testas12', 
    password_confirmation: 'testas12',
    first_name: "Administratorius",
    dob: "1990-12-31",
    location: "Vilnius",
    status: "childless",
    phone: "1234567890",
    avatar: ""
  })
end

# Artiles 
unless Article.any?
  puts "Creating articles"
  1.upto(20) do
    Article.create!({
      title: Faker::Lorem.sentence(3, true, 2), 
      body: Faker::Lorem.paragraph(rand(10..50)), 
      draft: false, 
      image: File.open(File.join(Rails.root + "app/assets/images/seed/#{rand(3)}.jpg"))
    }) 
  end
end

# Pages
unless Page.any?
  puts "Creating pages"
  1.upto(3) do
    Page.create!({
      title: Faker::Lorem.sentence(3, true, 2), 
      body: Faker::Lorem.paragraph(rand(50..100)),
      image: File.open(File.join(Rails.root + "app/assets/images/seed/#{rand(3)}.jpg")),
      menu: true,
      menu_label: Faker::Lorem.word
    })
  end
end

# Library
unless Material.any?
  puts "Creating materials and attachments"
  material = Material.create!({
    name: Faker::Lorem.sentence(3, true, 2), 
    body: Faker::Lorem.paragraph(rand(50..100)), 
    image: File.open(File.join(Rails.root + "app/assets/images/seed/0.jpg"))
  })

  material.material_attachments.create([
    {file: File.open(File.join(Rails.root + "app/assets/images/seed/sample.pdf"))},
    {file: File.open(File.join(Rails.root + "app/assets/images/seed/sample.mp3"))},
    {file: File.open(File.join(Rails.root + "app/assets/images/seed/sample.mp4"))}
  ])
end

# Topics
unless Topic.any?
  puts "Creating materials and attachments"
  user = User.find_by(email: "admin@todleris.lt")
  Topic.create!({
    user_id: user.id,
    subject: Faker::Lorem.sentence(3, true, 2), 
    body: Faker::Lorem.paragraph(rand(25..50)),
  })
end
